describe('Signup Page', () => {
  beforeEach(() => {
    cy.visit('/account/signup');
  });

  it('should have visible fields needed to signup and a disabled sign up button', () => {
    cy.get('[data-automation-id="firstNameInput"]').should('be.visible');
    cy.get('[data-automation-id="lastNameInput"]').should('be.visible');
    cy.get('[id="signupform-email"]').should('be.visible');
    cy.get('[data-automation-id="passwordInput"]').should('be.visible');
    cy.get('[data-automation-id="submit-signup"]').should('be.disabled');
  });

  it('should have a disabled sign up button when the form is partially filled out', () => {
    cy.get('[data-automation-id="firstNameInput"]').type('TestFirstName');
    cy.get('[data-automation-id="lastNameInput"]').type('TestLastName');
    cy.get('[id="signupform-email"]').type('no_password@sharklasers.com');
    cy.get('[data-automation-id="submit-signup"]').should('be.disabled');
  });

  it('should allow a user with valid information to signup for an account', () => {
    cy.intercept('/api/v1/mr/preference-center/preferences/preference-profile').as('prefProfile');
    cy.generateNewTestUserJson().then((newUser) => {
      cy.fillOutSignupForm(newUser);
      cy.get('[data-automation-id="submit-signup"]').click();
      cy.wait('@prefProfile');
      cy.contains('Verify Your Email');
      cy.contains(' Logout ');
    });
  });

  // ToDo: Test form validation
});