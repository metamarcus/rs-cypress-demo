describe('Auth Signup Endpoint', () => {
    it('should return 200 and a user id when a valid request is made', () => {
        cy.generateNewTestUserJson().then((userJson) => {
            cy.request('POST', '/api/v1/auth/sign-up', userJson).then((postResp) => {
                expect(postResp.status).to.eq(200);
            });
        });
    });

    describe('should return 400', () => {
        it('and an informative message when an invalid password is provided', () => {
            cy.generateNewTestUserJson().then((userJson) => {
                userJson.password = '';
                cy.request({
                    method: 'POST', 
                    url: '/api/v1/auth/sign-up', 
                    failOnStatusCode: false,
                    body: userJson
                }).then((postResp) => {
                    expect(postResp.status).to.eq(400);
                    expect(postResp.body.data).to.contain('There was a problem signing up');
                });
            });
        });
    });

    // ToDo: Json Body validation of other fields: firstName, lastName, and username aka email
});