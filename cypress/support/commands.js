Cypress.Commands.add('generateTestEmail', () => {
    var testEmail = 'rs_test_user_' + Math.floor((Math.random() * 100000) + 1) + '@sharklasers.com';
    cy.wrap(testEmail);
});

Cypress.Commands.add('generateNewTestUserJson', () => {
    cy.fixture('signup/new_user.json').then((newUser) => {
        cy.generateTestEmail().then(email => {
            newUser.username = email;
            cy.wrap(newUser);
        });
    });
});
  
  