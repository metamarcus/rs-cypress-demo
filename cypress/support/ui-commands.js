Cypress.Commands.add('fillOutSignupForm', (user) => {
    cy.get('[data-automation-id="firstNameInput"]').type(user.firstName);
    cy.get('[data-automation-id="lastNameInput"]').type(user.lastName);
    cy.get('[id="signupform-email"]').type(user.username);
    cy.get('[data-automation-id="passwordInput"]').type(user.password);
});